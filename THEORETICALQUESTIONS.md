1. Як можна оголосити змінну у Javascript?

Змінну у Javascript можна оголосити за допомогою слів let, const та var (зараз не використовується).

2. У чому різниця між функцією prompt та функцією confirm?

При використанні функції prompt відривається вікно із текстом та полем в яке користувач може записати текст, а при confirm відкривається вікно лише із якимось текстом (запитанням) де можливо лише обрати Ок чи Скасувати. prompt та confirm по різному взаємодіють з користувачем.

3. Що таке неявне перетворення типів? Наведіть один приклад.

Це коли автоматично відбувається перетворення типів без явної команди на зміну типу даних.

let a = 10;
let b = 3;  
let result = a / b;
console.log(result);

Відбудеться неявне перетворення цілого числа на число із плаваючою комою.
